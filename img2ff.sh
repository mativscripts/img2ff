#!/bin/bash

# img2ff 0.1 by mativ

# Function to convert image to farbfeld format
convert_image() {
    local image_path="$1"
    local output_path="${image_path%.*}.ff"

    if [[ $validity_check -eq 1 ]]; then
        if ! identify "$image_path" &> /dev/null; then
            echo "Invalid image: $image_path" >> "$log_file"
            errors=1
            return
        fi
    fi

    if [[ $quiet -ne 1 ]]; then
        echo "Processing image: $image_path"
    fi

    if [[ $compress -eq 1 ]]; then
        output_path="${output_path}.bz2"
        2ff < "$image_path" | bzip2 > "$output_path"
    else
        2ff < "$image_path" > "$output_path"
    fi

    if [[ ! -f "$output_path" ]]; then
        echo "Failed to create output file: $output_path" >> "$log_file"
        errors=1
    else
        if [[ $remove -eq 1 ]]; then
            rm "$image_path"
        fi
    fi
}

# Function to print usage manual
print_usage() {
    echo "Usage: mm2ff [-c] [-d directory_or_image ...] [-h|--help] [-q] [-l log_file] [-v] [-r]"
    echo "Options:"
    echo "  -c: Apply bzip2 compression to the output files."
    echo "  -d: Specify directories or individual images to be converted."
    echo "  -h, --help: Display this help message."
    echo "  -q: Quiet mode. No output will be printed."
    echo "  -l: Enable error logging. Specify a location for the log file."
    echo "  -v: Enable image validity check."
    echo "  -r: Remove the original file after conversion."
}

compress=0
quiet=0
errors=0
log_file=/dev/null
validity_check=0
remove=0

while getopts "cd:hql:vr" opt; do
    case ${opt} in
        c)
            compress=1
            ;;
        d)
            shift
            while (( "$#" )); do
                if [[ -d "$OPTARG" ]]; then
                    find "$OPTARG" -type f \( -iname \*.jpg -o -iname \*.png \) -exec bash -c "$(declare -f convert_image); compress=$compress; quiet=$quiet; log_file=$log_file; validity_check=$validity_check; remove=$remove; convert_image {}" \;
                elif [[ -f "$OPTARG" && ( "$OPTARG" == *.jpg || "$OPTARG" == *.png ) ]]; then
                    convert_image "$OPTARG"
                else
                    echo "Invalid directory or image: $OPTARG" >> "$log_file"
                    errors=1
                fi
                shift
            done
            ;;
        h)
            print_usage
            exit 0
            ;;
        q)
            quiet=1
            ;;
        l)
            log_file="$OPTARG"
            ;;
        v)
            validity_check=1
            ;;
        r)
            remove=1
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >> "$log_file"
            print_usage
            exit 1
            ;;
    esac
done

if [[ $errors -eq 1 && $quiet -ne 1 ]]; then
    echo "Some errors occurred during the execution." >> "$log_file"
fi