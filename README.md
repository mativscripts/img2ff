# img2ff

img2ff is a bash script that converts image files to the farbfeld format. It supports .jpg and .png files and allows for optional bzip2 compression. The script also includes options for quiet mode, error logging, image validity checking, and removal of the original file after conversion.

Useful for suckless sent, a program that I use to do presentations.

## Options

- -c: Apply bzip2 compression to the output files.
- -d: Specify directories or individual images to be converted.
- -h, --help: Display the help message.
- -q: Quiet mode. No output will be printed.
- -l: Enable error logging. Specify a location for the log file.
- -v: Enable image validity check.
- -r: Remove the original file after conversion.

## Dependencies

- farbfeld: A tool for converting images to the farbfeld format.
- bzip2: A freely available, patent free, high-quality data compressor.
- ImageMagick: To provide identify, used for image validity checking.

## Installation
```sh 
git clone https://gitlab.com/mativscripts/img2ff.git
cd img2ff
chmod +x img2ff.sh
sudo cp img2ff.sh /usr/local/bin/img2ff
img2ff -h
```

Version 0.1
License: GNU GPL v3.0.